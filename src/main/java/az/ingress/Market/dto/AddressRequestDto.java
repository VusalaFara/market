package az.ingress.Market.dto;

import az.ingress.Market.model.Market;
import az.ingress.Market.service.AddressService;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.Optional;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class AddressRequestDto{

   String addressName;

}
