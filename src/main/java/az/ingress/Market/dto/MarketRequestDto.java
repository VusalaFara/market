package az.ingress.Market.dto;

import az.ingress.Market.model.Branch;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class MarketRequestDto {

    @Size(min = 3,max = 20)
    String marketName;

    List<Branch> branch;

}
