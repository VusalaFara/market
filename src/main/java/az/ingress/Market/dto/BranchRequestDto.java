package az.ingress.Market.dto;

import az.ingress.Market.model.Address;
import az.ingress.Market.model.Market;
import jakarta.persistence.OneToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class BranchRequestDto {

    String branchName;

    Market market;

    Address address;

}
