package az.ingress.Market.controller;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.model.Market;
import az.ingress.Market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market")
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public Long createMarket( @Valid @RequestBody MarketRequestDto request){
         return marketService.createMarket(request);

    }
    @GetMapping("/{id}")
    public Market findById(@PathVariable Long id){
        return marketService.findById(id).get();
    }

    @GetMapping("/allmarkets")
    public List<Market> finAll(){
        return marketService.findAll();
    }
}
