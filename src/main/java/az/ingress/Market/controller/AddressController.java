package az.ingress.Market.controller;

import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressController {

    private final AddressService addressService;


    @PostMapping
    public Long createAddress(@RequestBody AddressRequestDto request) {
        return addressService.createAddress(request);
    }

    @GetMapping("/{id}")
    public Optional<Address> findById(@PathVariable Long id) {
        return addressService.findById(id);
    }

    @GetMapping("/alladdresses")
    public List<Address> findAll() {
        return addressService.findAll();
    }
}
