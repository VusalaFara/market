package az.ingress.Market.controller;

import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.model.Branch;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/branch")
public class BranchController {

   private final BranchService branchService;


   @PostMapping
    public Long createBranch(@RequestBody BranchRequestDto request){
        return branchService.createBranch(request);
}

    @GetMapping("/{id}")
    public Optional<Branch> findById(@PathVariable Long id) {
        return branchService.findById(id) ;
    }

    @GetMapping("/allbranches")
    public List<Branch> findAll() {
        return branchService.findAll();
    }
}
