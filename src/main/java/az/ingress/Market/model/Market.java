package az.ingress.Market.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity

public class Market {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String marketName;

    @OneToMany(mappedBy = "market",
    cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonIgnore
    List<Branch>branch;
}
