package az.ingress.Market.service;

import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;

import java.util.List;
import java.util.Optional;

public interface BranchService {

    Long createBranch(BranchRequestDto request);

    Optional<Branch> findById(Long id);

    List<Branch> findAll();
}
