package az.ingress.Market.service;

import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.model.Market;

import java.util.List;
import java.util.Optional;

public interface AddressService {

    Long createAddress(AddressRequestDto request);

    Optional<Address> findById(Long id);

    List<Address> findAll();
}
