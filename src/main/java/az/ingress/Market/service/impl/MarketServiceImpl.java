package az.ingress.Market.service.impl;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.AddressRepository;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.repository.MarketRepository;
import az.ingress.Market.service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final ModelMapper mapper;
    private final BranchRepository branchRepository;

    @Override
    public Long createMarket(MarketRequestDto request) {
        Market market = mapper.map(request, Market.class);
        for (Branch branch : request.getBranch())
        { log.error("branch is ,{}"+ branch);
            branch.setMarket(market);
            branchRepository.save(branch);
            marketRepository.save(market);

        }

            return market.getId();

    }

    @Override
    public Optional<Market> findById(Long id) {
        return marketRepository.findById(id);
    }

    @Override
    public List<Market> findAll() {
        return marketRepository.findAll();
    }
}
