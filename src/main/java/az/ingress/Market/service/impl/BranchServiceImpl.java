package az.ingress.Market.service.impl;

import az.ingress.Market.dto.BranchRequestDto;
import az.ingress.Market.model.Branch;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final ModelMapper mapper;


    @Override
    public Long createBranch(BranchRequestDto request) {
        Branch branch = mapper.map(request,Branch.class);
        branchRepository.save(branch);
        return branch.getId();
    }

    @Override
    public Optional<Branch> findById(Long id) {
        return branchRepository.findById(id) ;
    }

    @Override
    public List<Branch> findAll() {
        return branchRepository.findAll();
    }
}
