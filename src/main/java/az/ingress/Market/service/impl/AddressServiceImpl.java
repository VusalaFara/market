package az.ingress.Market.service.impl;

import az.ingress.Market.dto.AddressRequestDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.repository.AddressRepository;
import az.ingress.Market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final ModelMapper mapper;


    @Override
    public Long createAddress(AddressRequestDto request) {
        Address address = mapper.map(request,Address.class);
        addressRepository.save(address);
        return address.getId();
    }

    @Override
    public Optional<Address> findById(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public List<Address> findAll() {
        return addressRepository.findAll();
    }
}
