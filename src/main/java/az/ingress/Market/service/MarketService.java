package az.ingress.Market.service;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.model.Market;

import java.util.List;
import java.util.Optional;

public interface MarketService {
    Long createMarket(MarketRequestDto request);

    Optional<Market> findById(Long id);

    List<Market>findAll();
}
