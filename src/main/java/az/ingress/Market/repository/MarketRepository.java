package az.ingress.Market.repository;

import az.ingress.Market.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;



public interface MarketRepository extends JpaRepository<Market,Long> {
//    @Query(value = "SELECT m,b.branchName from Market m join m.Branch b on m.id = b.m_id")

}
